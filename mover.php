<?php
	require_once 'classes/dB.php';
	require_once 'classes/generic_partner_mover.php';
	require_once 'classes/kaltura_mover.php';
	require_once 'classes/newrow_user.php';
	require_once 'libs/dBug/dBug.php';
	// overall vision:
	// chron, every 5 - glue/controller of the partner_movers
	// (if there are no items marked as ‘in progress’ - meaning it’s currently running or broken)
	// fetch items from the database
	// create users array
	// create partners array
	// create batch array (foreach file in files)
	     // file, status, partner, user
	// mark dbRows as ‘in progress'
	// 
	// move the item to partner
	// if (user !in users array) var user = new user
	// if (partner !in partners array )var partner = new partner
	// partner::new_movement(user)
	// partner::move(new partner_move_file(file_from_db))
	// move to next file in files array
	// on complete, mark as complete or error in the dB
	
	/**
	 * function to send an email on error
	 */
	function sendErrorMessage($message) {
		mail('nathan@newrow.com', 'kaltura mover error', $message);
	}
	
	
	$partners_array;
	$users_array;
	
	$db = new dB();
	try {
		$db->connect();	
	}
	catch(Exeption $e) {
		//echo "there was an error connecting to the database";
		sendErrorMessage($db->printErrors());
	}
	
	
	/**
	 * fetch a batch of the queue items  
	 */
	$queueItems = $db->fetchQueueItems();
	
	//echo "queue items = <br /><pre>";
		//print_r($queueItems);
	//echo "</pre><br/>";
	
	
	/*
	 * loop through each of the items
	 * if the partner already exists (in the partners_array) then use that partner, do not create a new one
	 * if the partner does not already exist, create it.
	 */
	foreach ($queueItems as $thisItemToBeMoved) {
		switch ($thisItemToBeMoved->partner) {
			case 'kaltura':
				
				if (!isset($partners_array['kaltura'])) {
					//create a new partner for the move
					$partner_for_move = new Kaltura_mover();
					$partners_array['kaltura'] = $partner_for_move;
					//echo "partner = kaltura! <br />";	
				}
				else {
					$partner_for_move = $partners_array['kaltura'];
				}
				break;
		}
		////////////////////////////////////////
		// move the file
		////////////////////////////////////////
		/*
		 * if the user does not exist (in the users_array) then create a new user and insert it
		 * if the user does exist in the users_array, use that one
		 */
		if(!isset($users_array[$thisItemToBeMoved->partner_user_details_id])) {
			//create a new user
			$thisUser = new Newrow_user();
			$userDetails = $db->selectUserDetails($thisItemToBeMoved->partner_user_details_id, $thisItemToBeMoved->partner_list_id);
			
			
			$thisUser->setUsername($userDetails[0]->partner_username);
			$thisUser->setPass($userDetails[0]->partner_pass);
			$thisUser->setNewrow_user_id($userDetails[0]->newrow_user_id);
			$thisUser->setName('bobby');
			$thisUser->setId($userDetails[0]->id);
			$thisUser->setEnabled($userDetails[0]->enabled);
			$thisUser->setApi_secret($userDetails[0]->partner_api_secret);
			$thisUser->setApi_key($userDetails[0]->partner_api_key);
			$thisUser->setApi_id($userDetails[0]->partner_api_id);
			
			$users_array[$thisItemToBeMoved->partner_user_details_id] = $thisUser;
			
		}
		else {
			//use the existing user.
			// echo "using the existing user <br />";
			$thisUser = $users_array[$thisItemToBeMoved->partner_user_details_id];
		}
		
		/*
		 * mark this item as pending in the queue
		 */
		 try {
			$db->updateQueueItem($thisItemToBeMoved->id, 'pending');	
		}
		catch(Exeption $e) {
			sendErrorMessage('there was an error updating the database.' .$db->printErrors());
		}
		  
		
		//pass the user to the kaltura object
		/*
		 * set the user onto the partner
		 * and request that the file be moved
		 * 
		 */
		try {
			$partner_for_move->new_movement($thisUser);
			$partner_for_move->move('http://' .$thisItemToBeMoved->file);	
		}
		catch (Exception $e) {
			sendErrorMessage($e->getMessage());
		}
		
		
		/*
		 * update the queue with the 'complete' status when the queue item is done without errors 
		 */
		try {
			$db->updateQueueItem($thisItemToBeMoved->id, 'complete');	
		}
		catch(Exeption $e) {
			sendErrorMessage('there was an error updating the database.' .$db->printErrors());
		}
				
		//incredible debugging tool, much better than print_r
		//new dBug($thisItemToBeMoved);
	}
?>