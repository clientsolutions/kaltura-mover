# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: localhost (MySQL 5.6.19)
# Database: newrow_kaltura
# Generation Time: 2015-02-10 20:18:54 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table partner_list
# ------------------------------------------------------------

DROP TABLE IF EXISTS `partner_list`;

CREATE TABLE `partner_list` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `partner_list` WRITE;
/*!40000 ALTER TABLE `partner_list` DISABLE KEYS */;

INSERT INTO `partner_list` (`id`, `title`)
VALUES
	(1,'kaltura');

/*!40000 ALTER TABLE `partner_list` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table partner_user_details
# ------------------------------------------------------------

DROP TABLE IF EXISTS `partner_user_details`;

CREATE TABLE `partner_user_details` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `newrow_user_id` int(11) DEFAULT NULL,
  `partner_list_id` int(11) DEFAULT NULL,
  `partner_username` varchar(255) DEFAULT NULL,
  `partner_pass` varchar(255) DEFAULT NULL,
  `partner_api_id` varchar(255) DEFAULT NULL,
  `partner_api_key` varchar(255) DEFAULT NULL,
  `partner_api_secret` varchar(255) DEFAULT NULL,
  `enabled` int(11) DEFAULT NULL COMMENT '0,1,2 where 0 is no, 1 is yes, and 2 is ''per file''',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `partner_user_details` WRITE;
/*!40000 ALTER TABLE `partner_user_details` DISABLE KEYS */;

INSERT INTO `partner_user_details` (`id`, `newrow_user_id`, `partner_list_id`, `partner_username`, `partner_pass`, `partner_api_id`, `partner_api_key`, `partner_api_secret`, `enabled`)
VALUES
	(1,11436217,1,'nathan@watchitoo.com','yoshim00!','1829841',NULL,'1a071b1caaf20da3df97ff0e0b7d5c8c',1);

/*!40000 ALTER TABLE `partner_user_details` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table partner_xfer_queue
# ------------------------------------------------------------

DROP TABLE IF EXISTS `partner_xfer_queue`;

CREATE TABLE `partner_xfer_queue` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `partner_user_details_id` int(11) DEFAULT NULL,
  `partner_list_id` int(11) DEFAULT NULL COMMENT 'which partner to move to',
  `file` varchar(255) DEFAULT NULL COMMENT 'full url to the file that needs to be moved',
  `status` varchar(255) DEFAULT NULL COMMENT 'ready, in progress, complete or error',
  `status_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'time when the status was last updated',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `partner_xfer_queue` WRITE;
/*!40000 ALTER TABLE `partner_xfer_queue` DISABLE KEYS */;

INSERT INTO `partner_xfer_queue` (`id`, `partner_user_details_id`, `partner_list_id`, `file`, `status`, `status_time`)
VALUES
	(1,1,1,'cdn.newrow.com/download/content/userContent/11436217/5171baab05567/wdc-779_flv_1366407828541.flv',NULL,NULL),
	(2,1,1,'cdn.newrow.com/download/content/userContent/11436217/5127b77c0a717/whf-452_flv_1361557333212.flv',NULL,NULL),
	(3,1,1,'cdn.newrow.com/download/content/userContent/11436217/5171bb4eea172/wdc-779_flv_1366407958236.flv',NULL,NULL),
	(4,1,1,'cdn.newrow.com/download/content/userContent/11436217/51e056abb3da5/whf-452_3810897189_mov_1373656581032.mov',NULL,NULL),
	(5,1,1,'cdn.newrow.com/download/content/userContent/11436217/51e057b48bc20/whf-452_38108117210_mov_1373656771646.mov',NULL,NULL),
	(6,1,1,'cdn.newrow.com/download/content/userContent/11436217/51e5b636d6f1e/wus-349_3810897186_mov_1374004304082.mov',NULL,NULL),
	(7,1,1,'cdn.newrow.com/download/content/userContent/11436217/51e5ba31c43f1/wus-349_38108117216_mov_1374009126958.mov',NULL,NULL),
	(8,1,1,'cdn.newrow.com/download/content/userContent/11436217/51fbe36be73d2/wan-751_38108117210_mov_1375459646318.mov',NULL,NULL);

/*!40000 ALTER TABLE `partner_xfer_queue` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
