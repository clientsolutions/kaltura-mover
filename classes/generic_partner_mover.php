<?php
    class Generic_partner_mover {
    		
    	//constructor
		 function __construct() {
			//initialization here
		}
		 
		 //class vars
		protected $user;
		protected $file;
		 
		 
		 //class methods
		 /**
		  * prepares for a new movement of a file/asset to a partner based on the user
		  * accepts the user param, (class newrow_user) which should define
		  * connection details and destination information for the user/partner combination 
		  * 
		  */
		public function new_movement($user) {
			//setup with a user to create a new movement
			$this->$user = $user;
		}
		
		/**
		 * method to actually make the move of the passed in file
		 * this should assume that the newrow -> partner connection is successfully setup and ready
		 */
		public function move($partner_move_file) {
			//take a file in and move it to the partner
			$this->file = $partner_move_file;
		}
    }
?>