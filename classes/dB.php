<?php

/**
 * PDO database connection for the queue
 * 
 * the purpose of this class is to serve as a moderator between the other classes and the database,
 * offering some specific methods to get queue items, partner & user info, and update queue items
 * when they are completed
 * 
 * our guide through this new world of PDO (my first time using it) is
 * http://code.tutsplus.com/tutorials/why-you-should-be-using-phps-pdo-for-database-access--net-12059
 * 
 */
class dB {
	///////////////////////////////////////////////////////////////////////
	//// all the vars
	///////////////////////////////////////////////////////////////////////
	
	//local testing vars
	private $local_host = '127.0.0.1';
	private $local_dBname = 'newrow_kaltura';
	private $local_user = 'root';
	private $local_pass = '';
	
	//production vars
	private $production_host = '';
	private $production_dBname = '';
	private $production_user = '';
	private $production_pass = '';
	
	private $DBH = NULL;
	
	// are we local or production? localTesting or production
	private $system = '';
	
	//an array of all errors that can be retrievable
	private $dbErrors = NULL;
	
	
	
	///////////////////////////////////////////////////////////////////////
	//// constuctor
	///////////////////////////////////////////////////////////////////////
	/**
	 * assumes that we are connecting locally unless otherwise specified
	 */
	function __construct($environment = 'localTesting') {
		$system = $environment;
	}
	
	
	///////////////////////////////////////////////////////////////////////
	//// public functions
	///////////////////////////////////////////////////////////////////////
	/**
	 * checks to see if there is a connection and if not creates one
	 */	
	public function connect() {
		if (!isset($DBH)) {
			$this->createConnection();
		}
	}
	
	/**
	 * convenience method to print out all of the errors that this class
	 * added to the dbErrors array
	 * 
	 */
	public function printErrors() {
		if (isset($dbErrors)) {
			echo('<pre>');
				print_r($dbErrors);
			echo('</pre>');			
		}
		else {
			echo ("currently no db errors");
		}
	}
	
	
	//// queue handling
	/**
	 * fetches a number of queue items and returns the results as an object
	 */
	public function fetchQueueItems($numItems = 5) {
		try {
			$resultsObj = NULL;
			$query = "SELECT partner_xfer_queue.id, partner_xfer_queue.partner_user_details_id, partner_xfer_queue.partner_list_id, partner_xfer_queue.file, partner_xfer_queue.status, partner_xfer_queue.status_time, partner_list.title AS partner FROM partner_xfer_queue LEFT JOIN partner_list ON partner_list.id = partner_xfer_queue.partner_list_id WHERE partner_xfer_queue.status = 'ready' LIMIT " .$numItems;
			//echo "<br />query = " .$query ."<br/>";
			//echo "DBH = " .get_class($this->DBH) ."<br/>";
			$sth = $this->DBH->prepare($query);
			$sth->execute();
			
			$resultsObj = $sth->fetchAll(PDO::FETCH_OBJ);
			
			
		}
		catch (PDOException $e) {
			$dbErrors[] = $e->getMessage();
			throw new Exception('DB_ERROR - there was an error fetching the queue items');
		}
		
		return $resultsObj;
	}
	
	/**
	 * updates the queue item with a new status based on the row number passed in
	 */
	public function updateQueueItem($whichItem, $status) {
		//echo "<br />updating item" .$whichItem .' with the status of ' .$status .'<br />';
		try {	
			$data = array('id'=>$whichItem, 'status'=>$status);
			$sth = $this->DBH->prepare("UPDATE partner_xfer_queue SET status = :status WHERE id = :id LIMIT 1");
			$sth->execute($data);
			
			// echo "<pre>";
			// $sth->debugDumpParams();
			// echo "</pre>";
		}
		catch (PDOException $e){
			//echo "message = " .$e->getMessage();
			$dbErrors[] = $e->getMessage();
			throw new Exception('DB_ERROR - there was an error updating the queue item');
		}
	}
	
	
	//// user details handling
	/*
	 * pull the user details for the specified user/partner combination
	 * as a user can have details for multiple partners
	 */
	public function selectUserDetails($requestedUser, $requestedPartner) {
		$resultsObj = NULL;
		try{
			$data = array(':user'=>$requestedUser, ':partner'=>$requestedPartner);
			$sth = $this->DBH->prepare('SELECT * FROM partner_user_details WHERE id = :user AND partner_list_id = :partner LIMIT 1');
			$sth->setFetchMode(PDO::FETCH_OBJ);
			$sth->execute($data);
			$resultsObj = $sth->fetchAll();
			
			// echo "<pre>";
			// $sth->debugDumpParams();
			// echo "</pre>";
		}
		catch (PDOException $e) {
			$dbErrors[] = $e->getMessage();
			//echo $e->getMessage();
			throw new Exception('DB_ERROR - there was an error fetcing the user details');
		}
		
		return $resultsObj;
	}
	
	
	///////////////////////////////////////////////////////////////////////
	//// private functions
	///////////////////////////////////////////////////////////////////////
	/**
	 * creation of the initial connection (local or remote) depending on whether the 
	 * system has requested a local or production connection 
	 */
	private function createConnection() {
		
		//what are we connecting to?	
		if (($this->system == '') || ($this->system == 'localTesting')) {
			//echo "in local<br />";
			$host = $this->local_host;
			$dbName = $this->local_dBname;
			$user = $this->local_user;
			$pass = $this->local_pass;
			
		}
		else {
			//echo "in remote<br />";
			$host = $this->production_host;
			$dbName = $this->production_dBname;
			$user = $this->production_user;
			$pass = $this->production_pass;
		}
		
		
		//attempt to make the connection
		try {
			$this->DBH = new PDO("mysql:host=$host;dbname=$dbName", $user, $pass);
		}
		catch(PDOException $e) {
			//echo "db connection issue " .$e->getMessage();
			$dbErrors[] = $e->getMessage();
			throw new Exception('DB_ERROR - there was an error connection to the database');
		}
	}
}
?>