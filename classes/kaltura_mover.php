<?php
	//require the libraries
	require_once ('libs/kaltura/KalturaClient.php');
	require_once ('libs/dBug/dBug.php');

    class Kaltura_mover extends Generic_partner_mover {
    		
    	//constructor
		 function __construct() {
			//kaltura specific initialization here
		}
		 
		 //class vars
		protected $user;
		protected $file;
		
		protected $kalturaConfig;
		protected $kalturaClient;
		protected $ks;
		
		protected $mediaEntry;
		
		 //class methods
		 /**
		  * prepares for a new movement of a file/asset to a partner based on the user
		  * accepts the user param, (class newrow_user) which should define
		  * connection details and destination information for the user/partner combination 
		  * 
		  */

		 //class methods
		public function new_movement($user) {
			//setup with a user to create a new movement
			$this->user = $user;
			$this->setup();
		}
		
		
		/**
		 * method to setup the connection between newrow/partner
		 * when the user is set
		 */
		protected function setup() {
			$adminSecret = $this->user->getApi_secret();
			$userId = $this->user->getUsername();
			$partnerId = $this->user->getApi_id();
			$expiry = '31536000';
			try {
				$this->kalturaConfig = new KalturaConfiguration($partnerId);
				$this->kalturaClient = new KalturaClient($this->kalturaConfig);
				$this->ks = $this->kalturaClient->generateSession($adminSecret, $userId, KalturaSessionType::ADMIN, $partnerId, $expiry, $privileges);
				$this->kalturaClient->setKs($this->ks);
			}
			catch (Exception $e) {
				throw new Exception('MOVER_ERROR - there was an error attempting to create a new kaltura client when trying to move the file ' .$this->file .' to the users kaltura instance');
			}

		}
		
		
		/**
		 * method to actually make the move of the passed in file
		 * this should assume that the newrow -> partner connection is successfully setup and ready
		 */		
		public function move($partner_move_file) {
			$this->file = $partner_move_file;
			
			try {
				$this->mediaEntry = new KalturaMediaEntry();
				$this->mediaEntry->name = "newrow_ Imported Recording";  //this should or could be our title
				$this->mediaEntry->mediaType = KalturaMediaType::VIDEO;
				$this->mediaEntry->tags = 'newrow, recorded, recording, class';
			
				$newMediaResult = $this->kalturaClient->media->addFromUrl($this->mediaEntry, $this->file);
				
				// echo "------------------------------------------------------------------------------------<br />";
				// echo "------------------------------------------------------------------------------------<br />";
				// echo "new media result = <br />";
				// echo "------------------------------------------------------------------------------------<br />";
				// echo "------------------------------------------------------------------------------------<br />";
				// new dBug($newMediaResult);	
			}
			catch (Exception $e) {
				echo "there was an error in creating the media entry " .$e->getMessage();
				throw new Exception('MOVER_ERROR - there was an error attempting to send the file ' .$this->file .' to the users kaltura instance');
			}
			
	
		}
    }
?>