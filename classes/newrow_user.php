<?php
    class Newrow_user {
    		
    	//constructor
		 function __construct() {
			//initialization here
		}
		 
		 //class vars
		protected $name = '';
		protected $id = '';
		protected $newrow_user_id = '';
		protected $username = '';
		protected $pass = '';
		protected $api_key = '';
		protected $api_id = '';
		protected $api_secret = '';
		protected $enabled = '';
		 
		 //class methods
		 /*
		  * the methods listed below are all essentially getters and setters for the class
		  * all this is to do is hold a representation of the newrow customer and their associated credentials
		  * for the partners
		  */
		public function getName() {
			if ($this->name != '') {
				return $this->name;	
			}
			else {
				return null;
			}
		}
				
		public function getId() {
			if ($this->id != '') {
				return $this->id;
			}
			else {
				return null;
			}
		}
		
		public function getNewrow_user_id() {
			if ($this->newrow_user_id != '') {
				return $this->newrow_user_id;
			}
			else {
				return null;
			}
		}
		
		public function getUsername() {
			if ($this->username != '') {
				return $this->username;
			}
			else {
				return null;
			}
		}
		
		public function getPass() {
			if ($this->pass != '') {
				return $this->pass;
			}
			else {
				return null;
			}
		}
		
		public function getApi_key() {
			if ($this->api_key != '') {
				return $this->api_key;
			}
			else {
				return null;
			}
		}
		
		public function getApi_id() {
			if ($this->api_id != '') {
				return $this->api_id;
			}
			else {
				return null;
			}
		}
		
		public function getApi_secret() {
			if ($this->api_secret != '') {
				return $this->api_secret;
			}
			else {
				return null;
			}
		}
		
		public function getEnabled() {
			if ($this->enabled != '') {
				return $this->enabled;
			}
			else {
				return null;
			}
		}
		
		
		
		///////////////////////////////////////////////////////////////////////
		//// setters
		///////////////////////////////////////////////////////////////////////
		public function setName($incomingName) {
			$this->name = $incomingName;
		}
		
		public function setId($incomingId) {
			$this->id = $incomingId;
		}
		
		public function setNewrow_user_id($incomingUserId) {
			$this->newrow_user_id = $incomingUserId;
		}
		
		public function setUsername($incomingUserName) {
			$this->username = $incomingUserName;
		}
		
		public function setPass($incomingPass) {
			$this->pass = $incomingPass;
		}
		
		public function setApi_key($incomingApiKey) {
			$this->api_key = $incomingApiKey;
		}
		
		public function setApi_id($incomingApiId) {
			$this->api_id = $incomingApiId;
		}
		
		public function setApi_secret($incomingApiSecret) {
			$this->api_secret = $incomingApiSecret;
		}
		
		public function setEnabled($incomingEnabled) {
			$this->enabled = $incomingEnabled;
		}

    }
?>