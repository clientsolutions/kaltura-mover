<?php
    class Partner_move_file {
    		
    	//constructor
		 function __construct($incoming_url, $incoming_title) {
			//initialization here
			$file_url = $incoming_url;
		 	$file_title = $incoming_title;
		}
		 
		 //class vars
		protected $file_url = '';
		protected $file_title = '';
		 
		 
		 //class methods
		public function getTitle() {
			if ($file_title != '') {
				return $file_title;	
			}
			else {
				return null;
			}
		}
				
		public function getUrl() {
			if ($file_url != '') {
				return $file_url;
			}
			else {
				return null;
			}
		}

    }
?>