<?php
	/*
	 *
	 _what is this:
		this application is a connection and mover application to move files from newrow to (currently) Kaltura and hopefully others
	 	the application looks to a queue to get a batch of items and processes each of those items with the specified partner
	 
	     
	_entry points:
	 * 
	 * note: this is currently only working on my local machine.
	 * demos can be provided
	 * 
	dev
		currently none
	
	staging
		currently none
	
	prod
		currently none
	
	
	_how to install it:
		create the tables (see the sql folder)
	 	and create a chron job that runs at a 5 minute interval and calls mover.php
		this will kick off everything 
	
	_what (if any) third party apps does it use:
		none
	
	_how it works:
		chron kicks it off
	 	on the chron interval, mover.php grabs 5 items from the queue
	 	mover creates partners based on the queue items (currently only kaltura)
	 	mover gets user information for the newrow user/partner integration
	 	mover passes that user information to the partner
	 	partner sets up
	 	mover passes the file to the partner to move
	 	partner moves the file
	 	mover marks that file complete 
	
	_what databases does it use:
	 	the database tables do not yet exist on dev/staging/or production
	 	the tables that need to be created are in the sql folder of this project
	
	     
	 */
?>